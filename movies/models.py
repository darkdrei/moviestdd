from datetime import datetime

from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser

from movies.managers import MyUserManager


class CustomUser(AbstractUser):
    correo = models.EmailField(verbose_name='Correo',
                               max_length=255,
                               unique=True, )
    is_staff = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=datetime.now)
    ultima_conexicon = models.DateTimeField(auto_now_add=True)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = MyUserManager()

    @property
    def full_name(self):
        return '{} {}'.format(self.nombre or '', self.apellidos or '')

    def __str__(self):
        return self.correo

    def __unicode__(self):
        return '{} {} / {}'.format(self.nombre, self.apellidos, self.correo)